#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_j7duolte.mk

COMMON_LUNCH_CHOICES := \
    lineage_j7duolte-user \
    lineage_j7duolte-userdebug \
    lineage_j7duolte-eng
